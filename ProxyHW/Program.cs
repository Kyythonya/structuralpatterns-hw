﻿using System;

public interface IMoney
{
    void GetAmount();
}

class Banknote : IMoney
{
    public void GetAmount()
    {
        Console.WriteLine("Amount: one banknote");
    }
}

class CreditCard : IMoney
{
    private Banknote _banknote;

    public CreditCard(Banknote banknote)
    {
        _banknote = banknote;
    }

    public void GetAmount()
    {
        if (CheckAccess())
        {
            _banknote = new Banknote();
            _banknote.GetAmount();

            LogAccess();
        }
    }
    
    public bool CheckAccess()
    {
        Console.WriteLine("Proxy: Checking access prior to firing a real request.");

        return true;
    }

    public void LogAccess()
    {
        Console.WriteLine("Proxy: Logging the time of request.");
    }
}

namespace ProxyHW
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var creditCard = new CreditCard(new Banknote());

            creditCard.GetAmount();
            
            Console.ReadLine();
        }
    }
}
