﻿using System;

namespace AdapterHW
{
    public interface IRider
    {
        string Ride();
    }

    public class Rider : IRider
    {
        public ITransport Transport { get; set; }

        public Rider(ITransport transport)
        {
            Transport = transport;
        }

        public string Ride()
        {
            return $"Currently riding a {Transport.GetTransportType()}";
        }
    }

    public interface ITransport
    {
        string GetTransportType();
    }

    public class Camel : ITransport
    {
        public string GetTransportType()
        {
            return "camel";
        }
    }

    public class Bike : ITransport
    {
        public string GetTransportType()
        {
            return "bike";
        }
    }

    public class Horse : ITransport
    {
        public string GetTransportType()
        {
            return "horse";
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var rider = new Rider(new Horse());
            Console.WriteLine(rider.Ride());

            rider.Transport = new Bike();
            Console.WriteLine(rider.Ride());

            Console.ReadLine();
        }
    }
}
